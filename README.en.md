###  **docs
** 

 **introduce** 

docs is a document repository organized by the OpenHarmony Tripartite Component Library Private Repository to store OpenHarmony Tripartite Component Subproject documents.

 **Contribute code** 

Any problems found during the use of the process can be asked to issue to us, of course, we also very much welcome you to send us a PR.

 **Open source protocol** 

This project is based on the Apache License 2.0, so please feel free to enjoy and participate in open source.